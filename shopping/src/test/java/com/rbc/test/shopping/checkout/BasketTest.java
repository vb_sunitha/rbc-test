package com.rbc.test.shopping.checkout;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.rbc.test.shopping.checkout.Basket;
import com.rbc.test.shopping.item.Item;
import com.rbc.test.shopping.item.LineItem;
import com.rbc.test.shopping.item.ProductCode;
/**
 * 
 *
 * @author SB
 * @since 1.0
 */
public class BasketTest {
	private Basket<Item> basket;
	private Item banana;
	private Item orange;
	private Item peach;
	private Item apple;
	private Item lemon;

	@Before
	public void setUp()  {
		banana = new LineItem(ProductCode.BANANA.name(), new BigDecimal("0.50"));
		orange = new LineItem(ProductCode.ORANGE.name(), new BigDecimal("0.10"));
		lemon = new LineItem(ProductCode.LEMON.name(), new BigDecimal("0.05"));
		peach = new LineItem(ProductCode.PEACH.name(), new BigDecimal("0.25"));
		apple = new LineItem(ProductCode.APPLE.name(), new BigDecimal("0.40"));
		basket = new Basket<Item>();
	}

	@Test
	public void testGetItems() {
		// Given
		basket.addItem(banana, 2);

		// when
		HashMap<Item, Integer> items = basket.getItems();
		// Then
		assertThat(true, equalTo(items.containsKey(banana)));
		assertThat(2, equalTo(items.get(banana)));
	}

	@Test
	public void shouldAddUniqueItemsWithQuantity() {
		// Given
		basket.addItem(banana, 2);
		basket.addItem(orange, 10);
		basket.addItem(lemon, 5);
		basket.addItem(peach, 7);
		basket.addItem(apple, 10);

		// when
		HashMap<Item, Integer> items = basket.getItems();
		// Then
		assertThat(5, equalTo(items.size()));
		assertThat(true, equalTo(items.containsKey(banana)));
		assertThat(2, equalTo(items.get(banana)));
	}

	@Test
	public void shouldCalculateTotalCountWhenSameItemAddedTwice() {
		// Given
		basket.addItem(banana, 2);
		basket.addItem(banana, 4);

		// when
		HashMap<Item, Integer> items = basket.getItems();
		// Then
		assertThat(1, equalTo(items.size()));
		assertThat(true, equalTo(items.containsKey(banana)));
		assertThat(6, equalTo(items.get(banana)));
	}


	@Test
	public void shouldClearAllItemsFromTheBasket() {
		// Given
		basket.addItem(banana, 1);
		assertTrue(basket.getItems().size() > 0);
		// When
		basket.clear();
		// Then
		assertThat(0, equalTo(basket.getItems().size()));
	}

	@Test
	public void shouldDecrementCountWhenExistingItemIsRemovedFromBasket() {
		// Given
		basket.addItem(banana, 10);
		// when
		basket.removeItem(banana, 2);
		HashMap<Item, Integer> items = basket.getItems();

		// then
		assertThat(1, equalTo(items.size()));
		assertThat(true, equalTo(items.containsKey(banana)));
		assertThat(8, equalTo(items.get(banana)));
	}

	@Test
	public void shouldRemoveItemIfNetCountLessThanOrEqualtoZeroWhenExistingItemIsRemovedFromBasket() {
		// Given
		basket.addItem(banana, 2);
		basket.addItem(orange, 1);
		// when
		basket.removeItem(banana, 4);
		HashMap<Item, Integer> items = basket.getItems();

		// then
		assertThat(1, equalTo(items.size()));
		assertThat(false, equalTo(items.containsKey(banana)));
	}

	@Test
	public void shouldCalculateTotalOfAllItemsWithQuantity() {
		// Given
		basket.addItem(banana, 2);
		basket.addItem(orange, 4);
		basket.addItem(peach, 1);
		basket.addItem(lemon, 1);
		basket.addItem(apple, 5);
		// When
		BigDecimal total = basket.getTotal();

		// Then
		assertThat(total.toPlainString(), equalTo("3.70"));
	}

}
