/**
 * 
 */
package com.rbc.test.shopping.item;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * 
 *
 * @author SB
 * @since 1.0
 */
public class ItemTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExeptionWhenItemCreatedWithEmptyCode() {
        Item item  = new LineItem(" ",BigDecimal.TEN);
    }
    
    @Test(expected = NullPointerException.class)
    public void shouldThrowExeptionWhenItemCreatedWithNullCode() {
        Item item  = new LineItem(null,BigDecimal.TEN);
    }
    @Test(expected = NullPointerException.class)
    public void shouldThrowExeptionWhenItemCreatedWithNullPrice() {
        Item item  = new LineItem("Item",null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExeptionWhenItemCreatedWithNegetivePrice() {
        Item item  = new LineItem("Item",new BigDecimal("-1.00"));
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExeptionWhenItemCreatedWithZeroPrice() {
        Item item  = new LineItem("Item",BigDecimal.ZERO);
    }
    
    public void shouldCreateItemWithValidCodeAndPrice() {
        Item item  = new LineItem("Item",BigDecimal.TEN);
        assertNotNull(item);
    }
}
