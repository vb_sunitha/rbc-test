package com.rbc.test.shopping.checkout;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import com.rbc.test.shopping.checkout.Basket;
import com.rbc.test.shopping.checkout.Checkout;
import com.rbc.test.shopping.item.Item;
import com.rbc.test.shopping.item.LineItem;
import com.rbc.test.shopping.item.ProductCode;
/**
 * 
 *
 * @author SB
 * @since 1.0
 */
public class CheckOutTest {
	private Basket<Item> basket;
	private Item banana;
	private Item orange;
	private Item peach;
	private Item apple;
	private Item lemon;

	@Before
	public void setUp() throws Exception {
		banana = new LineItem(ProductCode.BANANA.name(), new BigDecimal("0.50"));
		orange = new LineItem(ProductCode.ORANGE.name(), new BigDecimal("0.10"));
		lemon = new LineItem(ProductCode.LEMON.name(), new BigDecimal("0.05"));
		peach = new LineItem(ProductCode.PEACH.name(), new BigDecimal("0.25"));
		apple = new LineItem(ProductCode.APPLE.name(), new BigDecimal("0.40"));
		basket = new Basket<Item>();
	}
	@Test
	public void ShouldCalculateTotalBasketWhenDiffrentProductsAreAdded() {
//		Given
		basket.addItem(banana, 2);
		basket.addItem(orange, 4);
		basket.addItem(peach, 1);
		basket.addItem(lemon, 1);
		basket.addItem(apple, 5);
		// When
		Checkout checkout = new Checkout(basket);
		BigDecimal total = checkout.getTotal();

		// Then
		assertThat(total.toPlainString(), equalTo("3.70"));
	}

}
