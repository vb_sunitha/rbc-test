package com.rbc.test.shopping.item;

/**
 * This class represent the items which can be shopped/added to the shopping
 * basket</br>
 * Its defined as an enum to keep it simple to match the given requirement(YAGNI)/br> 
 * In realLife, it wouldn't be an enum as it won't be extensible at runtime/br>
 * 
 * @author SB
 * @since 1.0
 *
 */
public enum ProductCode {
	BANANA("Banana"), APPLE("Apple"), ORANGE("Orange"), LEMON("Lemon"), PEACH("Peach");

	private String displayName;

	ProductCode(String name) {
		this.displayName = name;
	}

	public String getDisplayName() {
		return this.displayName;
	}
}
