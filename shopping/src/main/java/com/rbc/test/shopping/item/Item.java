package com.rbc.test.shopping.item;

import java.math.BigDecimal;
/**
 * This interface represent the Shopping Item
 * @author SB
 * @since 1.0
 *
 */
public interface Item {
	String getItemCode();
	BigDecimal getPrice();
}
