package com.rbc.test.shopping.item;

import java.math.BigDecimal;

import com.rbc.test.shopping.checkout.Basket;
/**
 * This class represent the Product thats will be added to the {@link Basket}
 * 
 * itemCode is assumed to be uniqueKey, other properties are omitted 
 * @author SB
 * @since 1.0
 */
public class LineItem implements Item {

	private final String itemCode;
	private final BigDecimal price;

	public LineItem(String itemCode, BigDecimal price) {
		validate(itemCode, price);
		this.itemCode = itemCode;
		this.price = price;
	}

	@Override
	public String getItemCode() {
		return itemCode;
	}

	@Override
	public BigDecimal getPrice() {
		return price;
	}
	
	/**
	 * @param itemCode
	 * @param price
	 */
	private void validate(String itemCode, BigDecimal price) {
		if(itemCode == null){
			throw new NullPointerException("ItemCode can't be null");
		}
		if(itemCode.trim().isEmpty()){
			throw new IllegalArgumentException("Invalid ItemCode");
		}
		if(price==null){
			throw new NullPointerException("Price can't be null");
		}
		else if( price.compareTo(BigDecimal.ZERO)<=0){
			throw new IllegalArgumentException("Invalid Price");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemCode == null) ? 0 : itemCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineItem other = (LineItem) obj;
		if (itemCode == null) {
			if (other.itemCode != null)
				return false;
		} else if (!itemCode.equals(other.itemCode))
			return false;
		return true;
	}
}
