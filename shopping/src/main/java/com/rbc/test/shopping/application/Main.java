package com.rbc.test.shopping.application;

import java.math.BigDecimal;
import com.rbc.test.shopping.checkout.Basket;
import com.rbc.test.shopping.checkout.Checkout;
import com.rbc.test.shopping.item.Item;
import com.rbc.test.shopping.item.LineItem;
import com.rbc.test.shopping.item.ProductCode;
/**
 * Main class to match the requirement as given:
 * Please write a program that takes a basket of items and outputs its total cost.
 * The basket can contain one or more of the following items: Bananas, Oranges, Apples, Lemons, Peaches
 * 
 * @author SB
 * @since 1.0
 */
public class Main {
	public static void main(String args[]){
		Item banana = new LineItem(ProductCode.BANANA.name(),new BigDecimal("0.50"));
		Item orenges = new LineItem(ProductCode.ORANGE.name(),new BigDecimal("0.10"));
		Item lemon = new LineItem(ProductCode.LEMON.name(),new BigDecimal("0.05"));
		Item peach = new LineItem(ProductCode.PEACH.name(),new BigDecimal("0.25"));
		Item apple = new LineItem(ProductCode.APPLE.name(), new BigDecimal("0.40"));

		Basket<Item> basket = new Basket<Item>();;
		basket.addItem(banana, 2);
		basket.addItem(orenges, 4);
		basket.addItem(peach, 1);
		basket.addItem(lemon, 1);
		basket.addItem(apple, 5);
		
		Checkout checkout = new Checkout(basket );
		BigDecimal total = checkout.getTotal();
		
		System.out.printf("Total for the checkout is :%s",total);
	}

}
