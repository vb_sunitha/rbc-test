package com.rbc.test.shopping.checkout;

import java.math.BigDecimal;

import com.rbc.test.shopping.item.Item;
/**
 * The Checkout takes in a {@link Basket} of {@link Item}s and calculate the total price
 *
 * @author SB
 * @since 1.0
 * 
 * @see Basket
 * @see Item
 */
public class Checkout {
	
	private final Basket<Item> basket;

	public Checkout(Basket<Item> basket) {
		this.basket = basket;
	}
	
    /**
     * Assumption: total price is rounded up two decimal places
     */
	public BigDecimal getTotal() {
		return this.basket.getItems().entrySet().stream()
				.map(entry->entry.getKey().getPrice().multiply(BigDecimal.valueOf(entry.getValue())))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}
