package com.rbc.test.shopping.checkout;

import java.math.BigDecimal;
import java.util.HashMap;

import com.rbc.test.shopping.item.Item;
import com.rbc.test.shopping.item.LineItem;
/**
 * Basket represent the collection of {@link Item} with quantity</br>
 * Assumption: 
 * 		Each checkout will get its own copy of the basket and hence the basket is not thread safe 
 *
 * @param <T>
 * @see Item
 * @see LineItem
 */
public class Basket<T extends Item> {
	
	private HashMap<T, Integer> orderLines;

    public HashMap<T, Integer> getItems() {
        return orderLines;
    }

    public Basket(){
        orderLines = new HashMap<>();
    }

    public  void  addItem(T item, int qty){
         if (orderLines.containsKey(item)) {
            orderLines.replace(item, orderLines.get(item) + qty);
        } else {
            orderLines.put(item,qty);
        }
    }
    
    public void removeItem(T item, int qty){
        if (orderLines.containsKey(item)) {
        	Integer currentQty = orderLines.get(item);
			int netQty = Math.max(0, currentQty-qty);
			if(netQty<=0){
			orderLines.remove(item);
			}else{
            orderLines.replace(item, netQty);
			}
        } 
    }

    public void clear(){
        orderLines.clear();
    }
    /**
     * Assumption: total price is rounded up two decimal places
     */
    public BigDecimal getTotal(){
    	return orderLines
        .keySet()
        .stream()
        .map( item -> item.getPrice().multiply(BigDecimal.valueOf(orderLines.get(item))))
        .reduce(BigDecimal.ZERO,BigDecimal::add).setScale(2,BigDecimal.ROUND_HALF_EVEN );
		}
	
}
